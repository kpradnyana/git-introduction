# Chapter 3 - Topic 3 - Git

## Tugas Perseorangan

- Student melakukan tahap git dibawah ini:
  - `git init` - Untuk menginisiasi git
  - `git add .` - Untuk menambahkan semua file perubahan ke dalam Git Staging Area
  - `git commit -m "Inisiasi git pertama"` - Untuk menetapkan perubahan yang ditambahkan sebelumnya di Staging Are ke Local Repository dengan pesan (entrypoint) "Inisiasi git pertama"
  - Membuat repository baru di Gitlab.com dengan akses public
  - Menyalin alamat HTTPS repository di Gitlab.com
  - Di lokal komputer, menjalankan perintah `git remote add origin [URL_GIT]` - Ganti [URL_GIT] dengan url yang di-copy dari gitlab.com
  - `git push origin master` - Untuk menyalin perubahan yang ada di Local Repository ke Remote Repository yang baru saja dibuat
- Student belajar konsep Branching dan Merge / Pull Request

## Tugas Berkelompok

Masing - masing kelompok terdiri dari 4 hingga 5 orang.
Dalam 1 tim akan dibagi menjadi beberapa role.

- Team Lead
- Full Stack Developer

Tugas

- Buka folder `kelompok` pada repository ini
- Masing - masing orang melakukan clone project yang akan dimodifikasi (`git clone [URL_GIT]`)
- Team Lead
  - Membuat file `index.html` dan `style.css` dengan konten yang sama seperti contoh pada file `kelompok/index.html` dan `kelompok/style.css`
  - Melakukan commit dan mem-push perubahannya ke remote repository
  - Menugaskan masing - masing Full Stack Developer pada tugas yang diberikan pada folder `kelompok/tasks`
  - Mereview perubahan dari Full Stack Developer dan memperikan Approval lalu melakukan Merge
  - Setelah melakukan merge, informasikan kepada Full Stack Developer untuk melakukan pull dari branch master agar tidak terjadi commit behind
- Full Stack Developer
  - Menerima tugas dari team lead
  - Membuat branch baru dengan menjalankan perintah `git checkout -b [TASK]` - ganti [TASK] dengan kode task yang diberikan Tean Lead
    - Contoh: `git checkout -b task-1`
  - Menyalin task yang diberikan di folder `kelompok/tasks` ke script yang ada di file `index.html` dan `style.css`
    - Contoh: FSD 1 menyalin TASK 1 ke script yang bertuliskan `<!-- TASK 1 -->`
  - Melakukan commit dan mem-push perubahannya ke remote repository
    - Contoh: `git push origin task-1`
  - Membuat Merge Request/Pull Request dan menunggu Approval dari Team Lead
  - Mereview code dari Full Stack Developer lain
